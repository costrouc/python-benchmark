import multiprocessing

import pytest
import numpy as np

@pytest.mark.benchmark(group='multiprocessing')
def test_process_pipe_latency_small_message(benchmark):
    def f(conn):
        i = 0
        while True:
            recv_message = conn.recv()
            if recv_message == 'quit':
                break
            conn.send(recv_message)
        child_conn.close()


    parent_conn, child_conn = multiprocessing.Pipe()
    p = multiprocessing.Process(target=f, args=(child_conn,))
    p.start()
    message = [1, 2, 3]

    def g():
        parent_conn.send(message)
        return parent_conn.recv()

    assert message == benchmark(g)
    parent_conn.send('quit')
    p.join()


@pytest.mark.benchmark(group='multiprocessing')
def test_process_pipe_latency_dftfit_message(benchmark):
    def reader_response(num_atoms):
        return {
            'stress': np.random.random((3, 3)),
            'forces': np.random.random((num_atoms, 3)),
            'energy': 1.0,
            'structure': np.random.random((num_atoms, 3))
        }
    back_message = [reader_response(100) for i in range(10)]

    def f(conn):
        i = 0
        while True:
            recv_message = conn.recv()
            if recv_message == 'quit':
                break
            conn.send(back_message)
        child_conn.close()


    parent_conn, child_conn = multiprocessing.Pipe()
    p = multiprocessing.Process(target=f, args=(child_conn,))
    p.start()

    message = {
        'version': 'v1',
        'kind': 'Potential',
        'spec': {
            'charge': {
                'Mg': 2.0,
                'O': -2.0
            },
            'kspace': {
                'type': 'pppm',
                'tollerance': 10.0
            },
            'pair': {
                'type': 'buckingham',
                'cutoff': 10.0,
                'parameters': [{
                    'elements': ['O', 'Mg'],
                    'coefficients': [1.0, 0.0, 3.0]
                }]
            }
        }
    }

    def g():
        parent_conn.send(message)
        return parent_conn.recv()

    result = benchmark(g)
    assert len(result) == len(back_message)
    parent_conn.send('quit')
    p.join()
