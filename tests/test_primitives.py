import pytest


@pytest.mark.benchmark(group='primitives')
def test_func_pass_only(benchmark):
    @benchmark
    def test():
        pass


@pytest.mark.benchmark(group='primitives')
def test_func_return_tuple(benchmark):
    @benchmark
    def test():
        return (1,)
