all: run
run:
	python3.6 -m benchmark
dump: clean
	python3.6 -m benchmark > results
	mpirun -n 2 python3.6 -m benchmark mpi >> results
clean:
	rm -f log.lammps pybench .tmp/*
