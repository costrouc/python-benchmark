==================
Python Performance
==================

An excursion to understand python performance better. Quickly written but welcome to improvements.

Tests:
 - primitives [for loop, if, equal, print]
 - asyncio
 - database [sqlite]
 - files [read, write, 1Kb, 1Mb]
 - logging [no opp, write to file]
 - mpi [req/rep]
 - programs [lammps]
 - random [choice]
 - serialize [msgpack, pickle]
 - sockets [req/rep, html req]
 - subprocess [python startup, overhead]
 - uuid [1, 4]
 - zmq [req/rep, async, bulk]
