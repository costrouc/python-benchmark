from .analyze import auto_timeit

uuid_setup = """
import uuid
"""

uuid1_create = """
id = uuid.uuid1()
"""

uuid4_create = """
id = uuid.uuid4()
"""

def tests():
    auto_timeit(uuid1_create, uuid_setup, 'uuid_uuid1_create')
    auto_timeit(uuid4_create, uuid_setup, 'uuid_uuid4_create')
