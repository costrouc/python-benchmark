from .analyze import auto_timeit

write_file_same = """
f = open('.tmp/example.txt', 'wb')
f.write(b'a'*{})
f.close()
"""

write_file_same_fsync = """
f = open('.tmp/example.txt', 'wb')
f.write(b'a'*{})
os.fsync(f.fileno())
f.close()
"""


write_file_random_setup = """
import uuid
import os
"""

write_file_random = """
f = open('.tmp/%s' % uuid.uuid4().hex, 'wb')
f.write(b'a'*{})
f.close()
"""

write_file_random_fsync = """
f = open('.tmp/%s' % uuid.uuid4().hex, 'wb')
f.write(b'a'*{})
os.fsync(f.fileno())
f.close()
"""

read_file_setup = """
import os
import random
files = os.listdir('./.tmp')
"""

read_file_same = """
v = random.choice(files)# to offset price of call
f = open('./.tmp/' + files[10], 'rb')
v = f.read()
f.close()
"""

read_file_random = """
f = open('./.tmp/' + random.choice(files), 'rb')
v = f.read()
f.close()
"""


def tests():
    auto_timeit(write_file_same.format(1000), write_file_random_setup, 'write_file_same_1Kb')
    auto_timeit(write_file_same_fsync.format(1000), write_file_random_setup, 'write_file_same_fsync_1Kb')
    auto_timeit(write_file_random.format(1000), write_file_random_setup, 'write_file_random_1Kb')
    auto_timeit(write_file_random_fsync.format(1000), write_file_random_setup, 'write_file_fsync_random_1Kb')

    auto_timeit(read_file_same, read_file_setup, 'read_file_same_1Kb')
    auto_timeit(read_file_random, read_file_setup, 'read_file_random_1Kb')

    auto_timeit(write_file_same.format(1_000_000), write_file_random_setup, 'write_file_same_1Mb')
    auto_timeit(write_file_same_fsync.format(1_000_000), write_file_random_setup, 'write_file_same_fsync_1Mb')
    auto_timeit(write_file_random.format(1_000_000), write_file_random_setup, 'write_file_random_1Mb')
    auto_timeit(write_file_random_fsync.format(1_000_000), write_file_random_setup, 'write_file_random_fsync_1Mb')
