from .analyze import auto_timeit

subprocess_setup = """
import subprocess
"""

python_startup_no_shell_no_site = """
subprocess.call(['python3.6', '-S', '-c', 'pass'])
"""

python_startup_no_shell = """
subprocess.call(['python3.6', '-c', 'pass'])
"""

python_startup_with_shell = """
subprocess.call(['python3.6', '-c', 'pass'])
"""

subprocess_call_time = """
subprocess.call(['true'])
"""

subprocess_call_time_with_shell = """
subprocess.call(['true'], shell=True)
"""


def tests():
    auto_timeit(python_startup_no_shell_no_site, subprocess_setup, "python_startup_no_shell_no_site")
    auto_timeit(python_startup_no_shell, subprocess_setup, "python_startup_no_shell")
    auto_timeit(python_startup_with_shell, subprocess_setup, "python_startup_with_shell")
    auto_timeit(subprocess_call_time, subprocess_setup, "subprocess_call_time")
    auto_timeit(subprocess_call_time_with_shell, subprocess_setup, "subprocess_call_time_with_shell")
