import msgpack
import json
import pickle

from .analyze import auto_timeit


msgpack_setup = """
import msgpack
DATA = {
    'input': b'a' * 1000,
    'files': {
        'initial.data': b'b' * 10_000
    }
}
dump = msgpack.dumps(DATA)
"""

msgpack_serialize = """
dump = msgpack.dumps(DATA)
"""

msgpack_deserialize = """
d = msgpack.loads(dump)
"""

pickle_setup = """
import pickle
DATA = {
    'input': b'a' * 1000,
    'files': {
        'initial.data': b'b' * 10_000
    }
}
dump = pickle.dumps(DATA)
"""

pickle_serialize = """
dump = pickle.dumps(DATA)
"""

pickle_deserialize = """
d = pickle.loads(dump)
"""


def tests():
    auto_timeit(msgpack_serialize, msgpack_setup, "msgpack_serialize_10Kb")
    auto_timeit(msgpack_deserialize, msgpack_setup, "msgpack_deserialize_10Kb")
    auto_timeit(pickle_serialize, pickle_setup, "pickle_serialize_10Kb")
    auto_timeit(pickle_deserialize, pickle_setup, "pickle_deserialize_10Kb")
