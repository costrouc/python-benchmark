import multiprocessing
import time
import socket

from .analyze import auto_timeit, analyze


page_request_setup = """
import urllib.request
"""

page_request = """
f = urllib.request.urlopen("https://google.com")
"""

def simple_socket_server(socket_type, address):
    try:
        sock = socket.socket(socket_type, socket.SOCK_STREAM)
        sock.bind(address)
        sock.listen(0)

        connection, address = sock.accept()
        message = connection.recv(512)
        assert message == b'ready'
        connection.sendall(b'ready')

        while True:
            message = connection.recv(512)
            connection.sendall(b'1')
    finally:
        sock.close()


def socket_client_req_rep(socket_type, address):
    process = multiprocessing.Process(target=simple_socket_server, args=(socket_type, address))
    process.start()
    time.sleep(1)

    sock = socket.socket(socket_type, socket.SOCK_STREAM)
    sock.connect(address)

    sock.sendall(b'ready')
    message = sock.recv(512)
    assert message == b'ready'

    start_time = time.perf_counter()
    for _ in range(100_000):
        sock.sendall(b'2')
        message = sock.recv(512)
    elapsed_time= time.perf_counter() - start_time
    if socket_type == socket.AF_INET:
        name = 'tcp'
    elif socket_type == socket.AF_UNIX:
        name = 'ipc'
    analyze("socket_%s_send_recv_round_trip" % name, (100_000, elapsed_time))
    process.terminate()

def tests():
    auto_timeit(page_request, page_request_setup, "webpage_request_google")
    socket_client_req_rep(socket.AF_INET, ('localhost', 8080)) # tcp socket
    socket_client_req_rep(socket.AF_UNIX, b'pybench') # ipc unix socket
