""" Notes

batch sending is how you get about 10X more performance
"""
import multiprocessing
import time
import asyncio
import functools

import zmq
import zmq.asyncio

from .analyze import analyze

PORT = 8000

def zmq_asyncio_wrapper_req_ipc_socket(n, queue, size):
    loop = zmq.asyncio.ZMQEventLoop()
    asyncio.set_event_loop(loop)
    queue.put(loop.run_until_complete(zmq_async_req_ipc_socket(n, size)))

async def zmq_async_req_ipc_socket(n, size=10):
    context = zmq.asyncio.Context()
    socket = context.socket(zmq.REQ)
    socket.bind("ipc://127.0.0.1:%s" % PORT)

    await socket.send(b'ready')
    message = await socket.recv()
    assert message == b'ready'

    start_time = time.perf_counter()
    for _ in range(n):
        await socket.send(b'0' * size)
        message = await socket.recv()
    elapsed_time = time.perf_counter() - start_time
    return (n, elapsed_time)

def zmq_asyncio_wrapper_rep_ipc_socket(n, size):
    loop = zmq.asyncio.ZMQEventLoop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(zmq_async_rep_ipc_socket(n, size))

async def zmq_async_rep_ipc_socket(n, size):
    context = zmq.asyncio.Context()
    socket = context.socket(zmq.REP)
    socket.connect("ipc://127.0.0.1:%s" % PORT)

    message = await socket.recv()
    assert message == b'ready'
    await socket.send(message)

    for _ in range(n):
        message = await socket.recv()
        await socket.send(message)

def zmq_asyncio_wrapper_dealer_tcp_socket(n, queue, size):
    loop = zmq.asyncio.ZMQEventLoop()
    asyncio.set_event_loop(loop)
    queue.put(loop.run_until_complete(zmq_async_dealer_tcp_socket(n, size)))

async def zmq_async_dealer_tcp_socket(n, size=10):
    context = zmq.asyncio.Context()
    socket = context.socket(zmq.DEALER)
    socket.bind("tcp://127.0.0.1:%s" % PORT)

    await socket.send(b'ready')
    message = await socket.recv()
    assert message == b'ready'

    start_time = time.perf_counter()
    for _ in range(n):
        await socket.send(b'0' * size)

    for _ in range(n):
        message = await socket.recv()
    elapsed_time = time.perf_counter() - start_time
    return (n, elapsed_time)

def zmq_asyncio_wrapper_router_tcp_socket(n, size):
    loop = zmq.asyncio.ZMQEventLoop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(zmq_async_router_tcp_socket(n, size))

async def zmq_async_router_tcp_socket(n, size):
    context = zmq.asyncio.Context()
    socket = context.socket(zmq.ROUTER)
    socket.connect("tcp://127.0.0.1:%s" % PORT)

    message = await socket.recv_multipart()
    assert message[1] == b'ready'
    await socket.send_multipart(message)

    for _ in range(n):
        message = await socket.recv_multipart()
        await socket.send_multipart(message)


def zmq_sync_req_ipc_socket(n, queue, size=10):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.bind("ipc://127.0.0.1:%s" % PORT)

    socket.send(b'ready')
    message = socket.recv()
    assert message == b'ready'

    start_time = time.perf_counter()
    for _ in range(n):
        socket.send(b'0' * size)
        message = socket.recv()
    elapsed_time = time.perf_counter() - start_time
    queue.put((n, elapsed_time))


def zmq_sync_rep_ipc_socket(n, size):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.connect("ipc://127.0.0.1:%s" % PORT)

    message = socket.recv()
    assert message == b'ready'
    socket.send(message)

    for _ in range(n):
        message = socket.recv()
        socket.send(message)


def zmq_sync_req_socket(n, queue, size=10):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.bind("tcp://127.0.0.1:%s" % PORT)

    socket.send(b'ready')
    message = socket.recv()
    assert message == b'ready'

    start_time = time.perf_counter()
    for _ in range(n):
        socket.send(b'0' * size)
        message = socket.recv()
    elapsed_time = time.perf_counter() - start_time
    queue.put((n, elapsed_time))


def zmq_sync_rep_socket(n, size):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.connect("tcp://127.0.0.1:%s" % PORT)

    message = socket.recv()
    assert message == b'ready'
    socket.send(message)

    for _ in range(n):
        message = socket.recv()
        socket.send(message)


def zmq_sync_dealer_socket_slow(n, queue, size=10):
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.bind("tcp://127.0.0.1:%s" % PORT)

    socket.send(b'ready')
    message = socket.recv()
    assert message == b'ready'

    start_time = time.perf_counter()
    for _ in range(n):
        socket.send(b'0' * size)
        message = socket.recv()
    elapsed_time = time.perf_counter() - start_time
    queue.put((n, elapsed_time))



def zmq_sync_dealer_socket(n, queue, size=10):
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.bind("tcp://127.0.0.1:%s" % PORT)

    socket.send(b'ready')
    message = socket.recv()
    assert message == b'ready'

    start_time = time.perf_counter()
    for _ in range(n):
        socket.send(b'0' * size)
    for _ in range(n):
        message = socket.recv()
    elapsed_time = time.perf_counter() - start_time
    queue.put((n, elapsed_time))


def zmq_sync_router_socket(n, size):
    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.connect("tcp://127.0.0.1:%s" % PORT)

    message = socket.recv_multipart()
    assert message[1] == b'ready'
    socket.send_multipart(message)

    for _ in range(n):
        message = socket.recv_multipart()
        socket.send_multipart(message)

def zmq_sync_dealer_ipc_socket(n, queue, size=10):
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.bind("ipc://127.0.0.1:%s" % (PORT+1))

    socket.send(b'ready')
    message = socket.recv()
    assert message == b'ready'

    start_time = time.perf_counter()
    for _ in range(n):
        socket.send(b'0' * size)
    for _ in range(n):
        message = socket.recv()
    elapsed_time = time.perf_counter() - start_time
    queue.put((n, elapsed_time))


def zmq_sync_router_ipc_socket(n, size):
    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.set_hwm(100_000)
    socket.connect("ipc://127.0.0.1:%s" % (PORT+1))

    message = socket.recv_multipart()
    assert message[1] == b'ready'
    socket.send_multipart(message)

    for _ in range(n):
        message = socket.recv_multipart()
        socket.send_multipart(message)


def zmq_test(master_f, slave_f, n, size):
    queue = multiprocessing.Queue()
    p1 = multiprocessing.Process(target=master_f, args=(n, queue, size))
    p2 = multiprocessing.Process(target=slave_f, args=(n, size))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
    return queue.get()


def tests():
    analyze('zmq_req_rep_async_ipc_real_latency', zmq_test(zmq_asyncio_wrapper_req_ipc_socket, zmq_asyncio_wrapper_rep_ipc_socket, 10_000, 0))
    analyze('zmq_dealer_router_async_tcp_send_at_once', zmq_test(zmq_asyncio_wrapper_dealer_tcp_socket, zmq_asyncio_wrapper_router_tcp_socket, 10_000, 0))
    analyze('zmq_req_rep_sync_ipc_real_latency', zmq_test(zmq_sync_req_ipc_socket, zmq_sync_rep_ipc_socket, 10_000, 0))
    analyze('zmq_req_rep_sync_tcp_real_latency', zmq_test(zmq_sync_req_socket, zmq_sync_rep_socket, 10_000, 0))
    analyze('zmq_dealer_router_sync_req_rep_slow', zmq_test(zmq_sync_dealer_socket_slow, zmq_sync_router_socket, 10_000, 0))
    analyze('zmq_dealer_router_sync_tcp_send_at_once', zmq_test(zmq_sync_dealer_socket, zmq_sync_router_socket, 10_000, 0))
    analyze('zmq_dealer_router_sync_ipc_send_at_once', zmq_test(zmq_sync_dealer_ipc_socket, zmq_sync_router_ipc_socket, 10_000, 0))
