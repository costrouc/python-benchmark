import time

from mpi4py import MPI

from .analyze import analyze


def mpi_send_recv(n):
    comm = MPI.COMM_WORLD
    if comm.size != 2:
        raise ValueError('mpi test requires size 2')

    time.sleep(1)

    if comm.rank == 0:
        start_time = time.perf_counter()
        for i in range(n):
            comm.send(None, dest=1)
            message = comm.recv(source=1)
        elapsed_time = time.perf_counter() - start_time
        analyze('mpi_send_recv_round_trip_latency', (n, elapsed_time))
    else:
        for i in range(n):
            message = comm.recv(source=0)
            comm.send(None, dest=0)



def tests():
    mpi_send_recv(10_000)
