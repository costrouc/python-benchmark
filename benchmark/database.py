# Postgres apparently can get up to about 8,000 inserts per second

from .analyze import auto_timeit

sqlite_memory_database_setup = '''
import sqlite3

connection = sqlite3.connect(":memory:")
cursor = connection.cursor()

cursor.execute("""
CREATE TABLE IF NOT EXISTS increment_table(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      i INTEGER NOT NULL
)
""")
'''

sqlite_file_database_setup = '''
import sqlite3
import datetime as dt

connection = sqlite3.connect("./.tmp/example.db")
cursor = connection.cursor()

cursor.execute("""
CREATE TABLE IF NOT EXISTS increment_table(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      i INTEGER NOT NULL
);
""")
'''

sqlite_insert = """
cursor.execute('INSERT INTO increment_table(i) VALUES(?);', (1,))
connection.commit()
"""

sqlite_select = """
result = cursor.execute('SELECT * FROM increment_table LIMIT 1;')
"""

sqlite_update = """
cursor.execute('UPDATE increment_table SET i = ? WHERE id = 10', (10, ))
connection.commit()
"""

def tests():
    auto_timeit(sqlite_insert, sqlite_memory_database_setup, "sqlite_memory_insert")
    auto_timeit(sqlite_select, sqlite_memory_database_setup + sqlite_insert * 100, "sqlite_memory_select")
    auto_timeit(sqlite_update, sqlite_memory_database_setup + sqlite_insert * 100, "sqlite_memory_update")
    auto_timeit(sqlite_insert, sqlite_file_database_setup, "sqlite_file_insert")
    auto_timeit(sqlite_select, sqlite_file_database_setup, "sqlite_file_select")
    auto_timeit(sqlite_update, sqlite_file_database_setup, "sqlite_file_update")
