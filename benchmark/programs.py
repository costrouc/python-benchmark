from .analyze import auto_timeit

subprocess_setup = """
import subprocess
"""

lammps_startup = """
subprocess.call(['lammps', '-i', '/dev/null'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
"""

def tests():
    auto_timeit(lammps_startup, subprocess_setup, "lammps_startup")
