import asyncio
import time
import uvloop

from .analyze import auto_async_timeit


async def async_func_pass():
    pass


async def async_sleep_milisecond():
    await asyncio.sleep(1e-3)


async def async_sleep_microsecond():
    await asyncio.sleep(1e-6)


def tests():
    auto_async_timeit(async_func_pass)
    auto_async_timeit(async_sleep_microsecond)
    auto_async_timeit(async_sleep_milisecond)

    # set uvloop
    loop = uvloop.new_event_loop()
    asyncio.set_event_loop(loop)

    auto_async_timeit(async_func_pass, name='uvloop_async_func_pass')
    auto_async_timeit(async_sleep_microsecond, name='uvloop_async_sleep_microsecond')
    auto_async_timeit(async_sleep_milisecond, name='uvloop_async_sleep_milisecond')

    # unset uvloop
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

