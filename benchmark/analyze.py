import timeit
import asyncio
import time

def auto_async_timeit(f, total_time=0.5, name=None):
    loop = asyncio.get_event_loop()

    async def _time_function(f, total_time=0.5):
        max_iterations = 1
        elapsed_time = 0
        while elapsed_time < total_time:
            start_time = time.perf_counter()
            for _ in range(max_iterations):
                await f()
            elapsed_time = time.perf_counter() - start_time
            if elapsed_time < total_time:
                max_iterations *= 10
        return max_iterations, elapsed_time

    t = loop.run_until_complete(_time_function(f, total_time))
    analyze(name or f.__name__, t)


def auto_timeit(stmt, setup='pass', name=None):
    t = timeit.Timer(stmt=stmt, setup=setup).autorange()
    if callable(stmt):
        name = stmt.__name__
    analyze(name, t)


def relative_time(t):
    if t < 1e-6:
        return f'{t * 1e9:>6.2f} [ns]'
    elif t < 1e-3:
        return f'{t * 1e6:>6.2f} [us]'
    elif t < 1:
        return f'{t * 1e3:>6.2f} [ms]'
    else:
        return f'{t:>6.2f}  [s]'


def analyze(name, t):
    print(f'{name:<64} - iter: {t[0]:>10} - {relative_time(t[1] / t[0])}')
