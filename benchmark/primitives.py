import time

from .analyze import auto_timeit, analyze


func_pass_only_setup = """
def func_pass_only():
    pass
"""

func_pass_only = """
func_pass_only()
"""

func_return_tuple_setup = """
def func_return_tuple():
    return (1,)
"""

func_return_tuple = """
value = func_return_tuple()
"""

increment_setup = """
i = 0
"""

increment = """
i += 1
"""

str_setup = """
strings = ['a' * 1000 for _ in range(10000)]
"""

str_no_join = """
total_string = ""
for s in strings:
    total_string += s
"""
str_join = """
total_string = "".join(strings)
"""

is_cost = """
_i is None
"""

equal_cost = """
_i == None
"""

if_cost = """
if _i is None:
    pass
"""

slots_setup = """
class Foo(object): __slots__ = 'foo',

class Bar(object): pass

slotted = Foo()
not_slotted = Bar()
"""

not_slotted = """
Bar.foo = 'foo'
Bar.foo
del Bar.foo
"""

slotted = """
Foo.foo = 'foo'
Foo.foo
del Foo.foo
"""

print_carridge_return = """
print(end="")
"""


def time_resolution_must_use_loop_for_smaller():
    elapsed_time = 0
    for _ in range(10_000):
        start_time, end_time = time.perf_counter(), time.perf_counter()
        elapsed_time += abs(end_time - start_time)
    analyze('time_resolution_must_use_loop_for_smaller', [1000, elapsed_time])


def tests():
    time_resolution_must_use_loop_for_smaller()
    auto_timeit("pass", "pass", "for_loop")
    auto_timeit(is_cost, "pass", "is_cost_subtract_for_loop")
    auto_timeit(equal_cost, "pass", "equal_cost_subtract_for_loop")
    auto_timeit(if_cost, "pass", "if_cost_subtract_for_and_is")
    auto_timeit(print_carridge_return, "pass", "print_carridge_return")
    auto_timeit(slotted, slots_setup, "slotted_get_set_del")
    auto_timeit(not_slotted, slots_setup, "not_slotted_get_set_del")
    auto_timeit(func_pass_only, func_pass_only_setup, "func_pass_only")
    auto_timeit(func_return_tuple, func_return_tuple_setup, "func_return_tuple")
    auto_timeit(increment, increment_setup, "increment_for_loop")
    auto_timeit(str_no_join, str_setup, "str_no_join_+")
    auto_timeit(str_join, str_setup, "str_join")
