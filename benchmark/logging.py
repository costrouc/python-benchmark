from .analyze import auto_timeit

logging_setup = """
import logging
logging.basicConfig(filename=".tmp/logging.log", level=logging.WARN)
logger = logging.getLogger(__name__)
"""

logging_no_print = """
logger.info('asdf')
"""


logging_to_file = """
logger.warn('asdf')
"""



def tests():
    auto_timeit(logging_no_print, logging_setup, 'logging_no_print_no_match')
    auto_timeit(logging_to_file, logging_setup, 'logging_print_to_file')
