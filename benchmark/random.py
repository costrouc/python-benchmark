from .analyze import auto_timeit


random_setup = """
import random
l = [_ for _ in range(10000)]
"""

random_choice = """
v = random.choice(l)
"""

def tests():
    auto_timeit(random_choice, random_setup, 'random_choice_10k')
