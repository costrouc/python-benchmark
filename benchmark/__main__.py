import sys

from .primitives import tests as primitives_tests
from .asyncio import tests as asyncio_tests
from .subprocess import tests as subprocess_tests
from .programs import tests as programs_tests
from .sockets import tests as sockets_tests
from .zmq import tests as zmq_tests
from .mpi import tests as mpi_tests
from .serialize import tests as serialize_tests
from .database import tests as database_tests
from .files import tests as files_tests
from .uuid import tests as uuid_tests
from .random import tests as random_tests
from .logging import tests as logging_tests

if __name__ == "__main__":
    available_timings = {
        'primitives': primitives_tests,
        'asyncio': asyncio_tests,
        'subprocess': subprocess_tests,
        'programs': programs_tests,
        'sockets': sockets_tests,
        'zmq': zmq_tests,
        'mpi': mpi_tests,
        'serialize': serialize_tests,
        'database': database_tests,
        'files': files_tests,
        'uuid': uuid_tests,
        'random': random_tests,
        'logging': logging_tests
    }
    default_timings = {'primitives', 'subprocess', 'programs', 'sockets', 'asyncio', 'zmq', 'serialize', 'database', 'random', 'files', 'uuid', 'logging'}

    if len(sys.argv) > 1:
        timings = set()
        for name in sys.argv[1:]:
            for key in available_timings:
                if name in key:
                    timings.add(key)
    else:
        timings = default_timings

    if 'mpi' in timings and len(timings) > 1:
        raise ValueError('mpi tests must be run separately with rank 2')

    for timing in timings:
        print(f'{timing.upper():=^20}')
        available_timings[timing]()
